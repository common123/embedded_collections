/** 
  ******************************************************************************
  * @file    lanKSZ8081.c
  * @author  MCD Application Team
  * @brief   This file provides a set of functions needed to manage the LAN742
  *          PHY devices.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "thirdparty/phy/microchip/lanKSZ8081/lanKSZ8081.h"

/** @addtogroup BSP
  * @{
  */

/** @addtogroup Component
  * @{
  */ 
  
/** @defgroup LAN8742 LAN8742
  * @{
  */   
  
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/** @defgroup LANKSZ8081_Private_Defines LAN8742 Private Defines
  * @{
  */
#define LANKSZ8081_SW_RESET_TO    ((uint32_t)500U)
#define LANKSZ8081_INIT_TO        ((uint32_t)2000U)
#define LANKSZ8081_MAX_DEV_ADDR   ((uint32_t)31U)
/**
  * @}
  */
 
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/** @defgroup LANKSZ8081_Private_Functions LAN8742 Private Functions
  * @{
  */
    
/**
  * @brief  Register IO functions to component object
  * @param  pObj: device object  of LANKSZ8081_Object_t.
  * @param  ioctx: holds device IO functions.  
  * @retval LANKSZ8081_STATUS_OK  if OK
  *         LANKSZ8081_STATUS_ERROR if missing mandatory function
  */
int32_t  LANKSZ8081_RegisterBusIO(lanKSZ8081_Object_t *pObj, lanKSZ8081_IOCtx_t *ioctx)
{
  if(!pObj || !ioctx->ReadReg || !ioctx->WriteReg || !ioctx->GetTick)
  {
    return LANKSZ8081_STATUS_ERROR;
  }
  
  pObj->IO.Init = ioctx->Init;
  pObj->IO.DeInit = ioctx->DeInit;
  pObj->IO.ReadReg = ioctx->ReadReg;
  pObj->IO.WriteReg = ioctx->WriteReg;
  pObj->IO.GetTick = ioctx->GetTick;
  
  return LANKSZ8081_STATUS_OK;
}

/**
  * @brief  Initialize the lanKSZ8081 and configure the needed hardware resources
  * @param  pObj: device object LANKSZ8081_Object_t.
  * @retval LANKSZ8081_STATUS_OK  if OK
  *         LANKSZ8081_STATUS_ADDRESS_ERROR if cannot find device address
  *         LANKSZ8081_STATUS_READ_ERROR if connot read register
  *         LANKSZ8081_STATUS_WRITE_ERROR if connot write to register
  *         LANKSZ8081_STATUS_RESET_TIMEOUT if cannot perform a software reset
  */
 int32_t LANKSZ8081_Init(lanKSZ8081_Object_t *pObj)
 {
   uint32_t tickstart = 0, regvalue = 0; // addr = 0;
   int32_t status = LANKSZ8081_STATUS_OK;
   
   if(pObj->Is_Initialized == 0)
   {
     if(pObj->IO.Init != 0)
     {
       /* GPIO and Clocks initialization */
       pObj->IO.Init();
     }

     /* Note HGS:
      * No need for specific phy address not considered in schematics anyway.
      */
     pObj->DevAddr = 0x0;

     ///* for later check */
     //pObj->DevAddr = LANKSZ8081_MAX_DEV_ADDR + 1;
     //
     ///* Get the device address from special mode register */
     //for(addr = 0; addr <= LANKSZ8081_MAX_DEV_ADDR; addr ++)
     //{
     //  if(pObj->IO.ReadReg(addr, LANKSZ8081_SMR, &regvalue) < 0)
     //  {
     //    status = LANKSZ8081_STATUS_READ_ERROR;
     //    /* Can't read from this device address
     //       continue with next address */
     //    continue;
     //  }
     //
     //  if((regvalue & LANKSZ8081_SMR_PHY_ADDR) == addr)
     //  {
     //    pObj->DevAddr = addr;
     //    status = LANKSZ8081_STATUS_OK;
     //    break;
     //  }
     //}
     //
     //if(pObj->DevAddr > LANKSZ8081_MAX_DEV_ADDR)
     //{
     //  status = LANKSZ8081_STATUS_ADDRESS_ERROR;
     //}
     
     /* if device address is matched */
     if(status == LANKSZ8081_STATUS_OK)
     {
       /* set a software reset  */
       if(pObj->IO.WriteReg(pObj->DevAddr, LANKSZ8081_BCR, LANKSZ8081_BCR_SOFT_RESET) >= 0)
       { 
         /* get software reset status */
         if(pObj->IO.ReadReg(pObj->DevAddr, LANKSZ8081_BCR, &regvalue) >= 0)
         { 
           tickstart = pObj->IO.GetTick();
           
           /* wait until software reset is done or timeout occured  */
           while(regvalue & LANKSZ8081_BCR_SOFT_RESET)
           {
             if((pObj->IO.GetTick() - tickstart) <= LANKSZ8081_SW_RESET_TO)
             {
               if(pObj->IO.ReadReg(pObj->DevAddr, LANKSZ8081_BCR, &regvalue) < 0)
               { 
                 status = LANKSZ8081_STATUS_READ_ERROR;
                 break;
               }
             }
             else
             {
               status = LANKSZ8081_STATUS_RESET_TIMEOUT;
               break;
             }
           } 
         }
         else
         {
           status = LANKSZ8081_STATUS_READ_ERROR;
         }
       }
       else
       {
         status = LANKSZ8081_STATUS_WRITE_ERROR;
       }
     }
   }
      
   if(status == LANKSZ8081_STATUS_OK)
   {
     tickstart =  pObj->IO.GetTick();
     
     /* Wait for 2s to perform initialization */
     while((pObj->IO.GetTick() - tickstart) <= LANKSZ8081_INIT_TO)
     {
     }
     pObj->Is_Initialized = 1;
   }
   
   return status;
 }

/**
  * @brief  De-Initialize the lanKSZ8081 and it's hardware resources
  * @param  pObj: device object LANKSZ8081_Object_t.
  * @retval None
  */
int32_t LANKSZ8081_DeInit(lanKSZ8081_Object_t *pObj)
{
  if(pObj->Is_Initialized)
  {
    if(pObj->IO.DeInit != 0)
    {
      if(pObj->IO.DeInit() < 0)
      {
        return LANKSZ8081_STATUS_ERROR;
      }
    }
  
    pObj->Is_Initialized = 0;  
  }
  
  return LANKSZ8081_STATUS_OK;
}

/**
  * @brief  Disable the LAN8742 power down mode.
  * @param  pObj: device object LANKSZ8081_Object_t.
  * @retval LANKSZ8081_STATUS_OK  if OK
  *         LANKSZ8081_STATUS_READ_ERROR if connot read register
  *         LANKSZ8081_STATUS_WRITE_ERROR if connot write to register
  */
int32_t LANKSZ8081_DisablePowerDownMode(lanKSZ8081_Object_t *pObj)
{
  uint32_t readval = 0;
  int32_t status = LANKSZ8081_STATUS_OK;
  
  if(pObj->IO.ReadReg(pObj->DevAddr, LANKSZ8081_BCR, &readval) >= 0)
  {
    readval &= ~LANKSZ8081_BCR_POWER_DOWN;
  
    /* Apply configuration */
    if(pObj->IO.WriteReg(pObj->DevAddr, LANKSZ8081_BCR, readval) < 0)
    {
      status =  LANKSZ8081_STATUS_WRITE_ERROR;
    }
  }
  else
  {
    status = LANKSZ8081_STATUS_READ_ERROR;
  }
   
  return status;
}

/**
  * @brief  Enable the LAN8742 power down mode.
  * @param  pObj: device object LANKSZ8081_Object_t.
  * @retval LANKSZ8081_STATUS_OK  if OK
  *         LANKSZ8081_STATUS_READ_ERROR if connot read register
  *         LANKSZ8081_STATUS_WRITE_ERROR if connot write to register
  */
int32_t LANKSZ8081_EnablePowerDownMode(lanKSZ8081_Object_t *pObj)
{
  uint32_t readval = 0;
  int32_t status = LANKSZ8081_STATUS_OK;
  
  if(pObj->IO.ReadReg(pObj->DevAddr, LANKSZ8081_BCR, &readval) >= 0)
  {
    readval |= LANKSZ8081_BCR_POWER_DOWN;
  
    /* Apply configuration */
    if(pObj->IO.WriteReg(pObj->DevAddr, LANKSZ8081_BCR, readval) < 0)
    {
      status =  LANKSZ8081_STATUS_WRITE_ERROR;
    }
  }
  else
  {
    status = LANKSZ8081_STATUS_READ_ERROR;
  }
   
  return status;
}

/**
  * @brief  Start the auto negotiation process.
  * @param  pObj: device object LANKSZ8081_Object_t.
  * @retval LANKSZ8081_STATUS_OK  if OK
  *         LANKSZ8081_STATUS_READ_ERROR if connot read register
  *         LANKSZ8081_STATUS_WRITE_ERROR if connot write to register
  */
int32_t LANKSZ8081_StartAutoNego(lanKSZ8081_Object_t *pObj)
{
  uint32_t readval = 0;
  int32_t status = LANKSZ8081_STATUS_OK;
  
  if(pObj->IO.ReadReg(pObj->DevAddr, LANKSZ8081_BCR, &readval) >= 0)
  {
    readval |= LANKSZ8081_BCR_AUTONEGO_EN;
  
    /* Apply configuration */
    if(pObj->IO.WriteReg(pObj->DevAddr, LANKSZ8081_BCR, readval) < 0)
    {
      status =  LANKSZ8081_STATUS_WRITE_ERROR;
    }
  }
  else
  {
    status = LANKSZ8081_STATUS_READ_ERROR;
  }
   
  return status;
}

/**
  * @brief  Get the link state of LAN8742 device.
  * @param  pObj: Pointer to device object. 
  * @param  pLinkState: Pointer to link state
  * @retval LANKSZ8081_STATUS_LINK_DOWN  if link is down
  *         LANKSZ8081_STATUS_AUTONEGO_NOTDONE if Auto nego not completed
  *         LANKSZ8081_STATUS_100MBITS_FULLDUPLEX if 100Mb/s FD
  *         LANKSZ8081_STATUS_100MBITS_HALFDUPLEX if 100Mb/s HD
  *         LANKSZ8081_STATUS_10MBITS_FULLDUPLEX  if 10Mb/s FD
  *         LANKSZ8081_STATUS_10MBITS_HALFDUPLEX  if 10Mb/s HD
  *         LANKSZ8081_STATUS_READ_ERROR if connot read register
  *         LANKSZ8081_STATUS_WRITE_ERROR if connot write to register
  */
int32_t LANKSZ8081_GetLinkState(lanKSZ8081_Object_t *pObj)
{
  uint32_t readval = 0;
  
  /* Read Status register  */
  if(pObj->IO.ReadReg(pObj->DevAddr, LANKSZ8081_BSR, &readval) < 0)
  {
    return LANKSZ8081_STATUS_READ_ERROR;
  }
  
  /* Read Status register again */
  if(pObj->IO.ReadReg(pObj->DevAddr, LANKSZ8081_BSR, &readval) < 0)
  {
    return LANKSZ8081_STATUS_READ_ERROR;
  }
  
  if((readval & LANKSZ8081_BSR_LINK_STATUS) == 0)
  {
    /* Return Link Down status */
    return LANKSZ8081_STATUS_LINK_DOWN;
  }
  
  /* Check Auto negotiaition */
  if(pObj->IO.ReadReg(pObj->DevAddr, LANKSZ8081_BCR, &readval) < 0)
  {
    return LANKSZ8081_STATUS_READ_ERROR;
  }
  
  if((readval & LANKSZ8081_BCR_AUTONEGO_EN) != LANKSZ8081_BCR_AUTONEGO_EN)
  {
    if(((readval & LANKSZ8081_BCR_SPEED_SELECT) == LANKSZ8081_BCR_SPEED_SELECT) && ((readval & LANKSZ8081_BCR_DUPLEX_MODE) == LANKSZ8081_BCR_DUPLEX_MODE))
    {
      return LANKSZ8081_STATUS_100MBITS_FULLDUPLEX;
    }
    else if ((readval & LANKSZ8081_BCR_SPEED_SELECT) == LANKSZ8081_BCR_SPEED_SELECT)
    {
      return LANKSZ8081_STATUS_100MBITS_HALFDUPLEX;
    }        
    else if ((readval & LANKSZ8081_BCR_DUPLEX_MODE) == LANKSZ8081_BCR_DUPLEX_MODE)
    {
      return LANKSZ8081_STATUS_10MBITS_FULLDUPLEX;
    }
    else
    {
      return LANKSZ8081_STATUS_10MBITS_HALFDUPLEX;
    }  		
  }
  else /* Auto Nego enabled */
  {
    if(pObj->IO.ReadReg(pObj->DevAddr, LANKSZ8081_PHYCTRL1, &readval) < 0)
    {
      return LANKSZ8081_STATUS_READ_ERROR;
    }
    
    /* Check if auto nego not done */
    if((readval & LANKSZ8081_PHYCTRL1_AUTONEGO_ONGOING) == LANKSZ8081_PHYCTRL1_AUTONEGO_ONGOING)
    {
      return LANKSZ8081_STATUS_AUTONEGO_NOTDONE;
    }
    
    if((readval & LANKSZ8081_PHYCTRL1_HCDSPEEDMASK) == LANKSZ8081_PHYCTRL1_100BTX_FD)
    {
      return LANKSZ8081_STATUS_100MBITS_FULLDUPLEX;
    }
    else if ((readval & LANKSZ8081_PHYCTRL1_HCDSPEEDMASK) == LANKSZ8081_PHYCTRL1_100BTX_HD)
    {
      return LANKSZ8081_STATUS_100MBITS_HALFDUPLEX;
    }
    else if ((readval & LANKSZ8081_PHYCTRL1_HCDSPEEDMASK) == LANKSZ8081_PHYCTRL1_10BT_FD)
    {
      return LANKSZ8081_STATUS_10MBITS_FULLDUPLEX;
    }
    else
    {
      return LANKSZ8081_STATUS_10MBITS_HALFDUPLEX;
    }				
  }
}

/**
  * @brief  Set the link state of LAN8742 device.
  * @param  pObj: Pointer to device object. 
  * @param  pLinkState: link state can be one of the following
  *         LANKSZ8081_STATUS_100MBITS_FULLDUPLEX if 100Mb/s FD
  *         LANKSZ8081_STATUS_100MBITS_HALFDUPLEX if 100Mb/s HD
  *         LANKSZ8081_STATUS_10MBITS_FULLDUPLEX  if 10Mb/s FD
  *         LANKSZ8081_STATUS_10MBITS_HALFDUPLEX  if 10Mb/s HD
  * @retval LANKSZ8081_STATUS_OK  if OK
  *         LANKSZ8081_STATUS_ERROR  if parameter error
  *         LANKSZ8081_STATUS_READ_ERROR if connot read register
  *         LANKSZ8081_STATUS_WRITE_ERROR if connot write to register
  */
int32_t LANKSZ8081_SetLinkState(lanKSZ8081_Object_t *pObj, uint32_t LinkState)
{
  uint32_t bcrvalue = 0;
  int32_t status = LANKSZ8081_STATUS_OK;
  
  if(pObj->IO.ReadReg(pObj->DevAddr, LANKSZ8081_BCR, &bcrvalue) >= 0)
  {
    /* Disable link config (Auto nego, speed and duplex) */
    bcrvalue &= ~(LANKSZ8081_BCR_AUTONEGO_EN | LANKSZ8081_BCR_SPEED_SELECT | LANKSZ8081_BCR_DUPLEX_MODE);
    
    if(LinkState == LANKSZ8081_STATUS_100MBITS_FULLDUPLEX)
    {
      bcrvalue |= (LANKSZ8081_BCR_SPEED_SELECT | LANKSZ8081_BCR_DUPLEX_MODE);
    }
    else if (LinkState == LANKSZ8081_STATUS_100MBITS_HALFDUPLEX)
    {
      bcrvalue |= LANKSZ8081_BCR_SPEED_SELECT;
    }
    else if (LinkState == LANKSZ8081_STATUS_10MBITS_FULLDUPLEX)
    {
      bcrvalue |= LANKSZ8081_BCR_DUPLEX_MODE;
    }
    else
    {
      /* Wrong link status parameter */
      status = LANKSZ8081_STATUS_ERROR;
    }	
  }
  else
  {
    status = LANKSZ8081_STATUS_READ_ERROR;
  }
  
  if(status == LANKSZ8081_STATUS_OK)
  {
    /* Apply configuration */
    if(pObj->IO.WriteReg(pObj->DevAddr, LANKSZ8081_BCR, bcrvalue) < 0)
    {
      status = LANKSZ8081_STATUS_WRITE_ERROR;
    }
  }
  
  return status;
}

/**
  * @brief  Enable loopback mode.
  * @param  pObj: Pointer to device object. 
  * @retval LANKSZ8081_STATUS_OK  if OK
  *         LANKSZ8081_STATUS_READ_ERROR if connot read register
  *         LANKSZ8081_STATUS_WRITE_ERROR if connot write to register
  */
int32_t LANKSZ8081_EnableLoopbackMode(lanKSZ8081_Object_t *pObj)
{
  uint32_t readval = 0;
  int32_t status = LANKSZ8081_STATUS_OK;
  
  if(pObj->IO.ReadReg(pObj->DevAddr, LANKSZ8081_BCR, &readval) >= 0)
  {
    readval |= LANKSZ8081_BCR_LOOPBACK;
    
    /* Apply configuration */
    if(pObj->IO.WriteReg(pObj->DevAddr, LANKSZ8081_BCR, readval) < 0)
    {
      status = LANKSZ8081_STATUS_WRITE_ERROR;
    }
  }
  else
  {
    status = LANKSZ8081_STATUS_READ_ERROR;
  }
  
  return status;
}

/**
  * @brief  Disable loopback mode.
  * @param  pObj: Pointer to device object. 
  * @retval LANKSZ8081_STATUS_OK  if OK
  *         LANKSZ8081_STATUS_READ_ERROR if connot read register
  *         LANKSZ8081_STATUS_WRITE_ERROR if connot write to register
  */
int32_t LANKSZ8081_DisableLoopbackMode(lanKSZ8081_Object_t *pObj)
{
  uint32_t readval = 0;
  int32_t status = LANKSZ8081_STATUS_OK;
  
  if(pObj->IO.ReadReg(pObj->DevAddr, LANKSZ8081_BCR, &readval) >= 0)
  {
    readval &= ~LANKSZ8081_BCR_LOOPBACK;
  
    /* Apply configuration */
    if(pObj->IO.WriteReg(pObj->DevAddr, LANKSZ8081_BCR, readval) < 0)
    {
      status =  LANKSZ8081_STATUS_WRITE_ERROR;
    }
  }
  else
  {
    status = LANKSZ8081_STATUS_READ_ERROR;
  }
   
  return status;
}

/**
  * @brief  Enable IT source.
  * @param  pObj: Pointer to device object. 
  * @param  Interrupt: IT source to be enabled
  *         should be a value or a combination of the following:
  *         LANKSZ8081_WOL_IT
  *         LANKSZ8081_ENERGYON_IT
  *         LANKSZ8081_AUTONEGO_COMPLETE_IT
  *         LANKSZ8081_REMOTE_FAULT_IT
  *         LANKSZ8081_LINK_DOWN_IT
  *         LANKSZ8081_AUTONEGO_LP_ACK_IT
  *         LANKSZ8081_PARALLEL_DETECTION_FAULT_IT
  *         LANKSZ8081_AUTONEGO_PAGE_RECEIVED_IT
  * @retval LANKSZ8081_STATUS_OK  if OK
  *         LANKSZ8081_STATUS_READ_ERROR if connot read register
  *         LANKSZ8081_STATUS_WRITE_ERROR if connot write to register
  */
int32_t LANKSZ8081_EnableIT(lanKSZ8081_Object_t *pObj, uint32_t Interrupt)
{
  uint32_t readval = 0;
  int32_t status = LANKSZ8081_STATUS_OK;
  
  if(pObj->IO.ReadReg(pObj->DevAddr, LANKSZ8081_ICSR, &readval) >= 0)
  {
    readval |= Interrupt;
  
    /* Apply configuration */
    if(pObj->IO.WriteReg(pObj->DevAddr, LANKSZ8081_ICSR, readval) < 0)
    {
      status =  LANKSZ8081_STATUS_WRITE_ERROR;
    }
  }
  else
  {
    status = LANKSZ8081_STATUS_READ_ERROR;
  }
   
  return status;
}

/**
  * @brief  Disable IT source.
  * @param  pObj: Pointer to device object. 
  * @param  Interrupt: IT source to be disabled
  *         should be a value or a combination of the following:
  *         LANKSZ8081_WOL_IT
  *         LANKSZ8081_ENERGYON_IT
  *         LANKSZ8081_AUTONEGO_COMPLETE_IT
  *         LANKSZ8081_REMOTE_FAULT_IT
  *         LANKSZ8081_LINK_DOWN_IT
  *         LANKSZ8081_AUTONEGO_LP_ACK_IT
  *         LANKSZ8081_PARALLEL_DETECTION_FAULT_IT
  *         LANKSZ8081_AUTONEGO_PAGE_RECEIVED_IT
  * @retval LANKSZ8081_STATUS_OK  if OK
  *         LANKSZ8081_STATUS_READ_ERROR if connot read register
  *         LANKSZ8081_STATUS_WRITE_ERROR if connot write to register
  */
int32_t LANKSZ8081_DisableIT(lanKSZ8081_Object_t *pObj, uint32_t Interrupt)
{
  uint32_t readval = 0;
  int32_t status = LANKSZ8081_STATUS_OK;
  
  if(pObj->IO.ReadReg(pObj->DevAddr, LANKSZ8081_ICSR, &readval) >= 0)
  {
    readval &= ~Interrupt;
  
    /* Apply configuration */
    if(pObj->IO.WriteReg(pObj->DevAddr, LANKSZ8081_ICSR, readval) < 0)
    {
      status = LANKSZ8081_STATUS_WRITE_ERROR;
    }
  }
  else
  {
    status = LANKSZ8081_STATUS_READ_ERROR;
  }
   
  return status;
}

/**
  * @brief  Clear IT flag.
  * @param  pObj: Pointer to device object. 
  * @param  Interrupt: IT flag to be cleared
  *         should be a value or a combination of the following:
  *         LANKSZ8081_WOL_IT
  *         LANKSZ8081_ENERGYON_IT
  *         LANKSZ8081_AUTONEGO_COMPLETE_IT
  *         LANKSZ8081_REMOTE_FAULT_IT
  *         LANKSZ8081_LINK_DOWN_IT
  *         LANKSZ8081_AUTONEGO_LP_ACK_IT
  *         LANKSZ8081_PARALLEL_DETECTION_FAULT_IT
  *         LANKSZ8081_AUTONEGO_PAGE_RECEIVED_IT
  * @retval LANKSZ8081_STATUS_OK  if OK
  *         LANKSZ8081_STATUS_READ_ERROR if connot read register
  */
int32_t  LANKSZ8081_ClearIT(lanKSZ8081_Object_t *pObj, uint32_t Interrupt)
{
  uint32_t readval = 0;
  int32_t status = LANKSZ8081_STATUS_OK;
  
  if(pObj->IO.ReadReg(pObj->DevAddr, LANKSZ8081_ICSR, &readval) < 0)
  {
    status =  LANKSZ8081_STATUS_READ_ERROR;
  }
  
  return status;
}

/**
  * @brief  Get IT Flag status.
  * @param  pObj: Pointer to device object. 
  * @param  Interrupt: IT Flag to be checked, 
  *         should be a value or a combination of the following:
  *         LANKSZ8081_WOL_IT
  *         LANKSZ8081_ENERGYON_IT
  *         LANKSZ8081_AUTONEGO_COMPLETE_IT
  *         LANKSZ8081_REMOTE_FAULT_IT
  *         LANKSZ8081_LINK_DOWN_IT
  *         LANKSZ8081_AUTONEGO_LP_ACK_IT
  *         LANKSZ8081_PARALLEL_DETECTION_FAULT_IT
  *         LANKSZ8081_AUTONEGO_PAGE_RECEIVED_IT
  * @retval 1 IT flag is SET
  *         0 IT flag is RESET
  *         LANKSZ8081_STATUS_READ_ERROR if connot read register
  */
int32_t LANKSZ8081_GetITStatus(lanKSZ8081_Object_t *pObj, uint32_t Interrupt)
{
  uint32_t readval = 0;
  int32_t status = 0;

  if(pObj->IO.ReadReg(pObj->DevAddr, LANKSZ8081_ICSR, &readval) >= 0)
  {
    status = ((readval & Interrupt) == Interrupt);
  }
  else
  {
    status = LANKSZ8081_STATUS_READ_ERROR;
  }
	
  return status;
}

/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */      
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

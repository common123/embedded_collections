/**
  ******************************************************************************
  * @file    lanKSZ8081.h
  * @author  MCD Application Team
  * @brief   This file contains all the functions prototypes for the
  *          lanKSZ8081.c PHY driver.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef LANKSZ8081_H
#define LANKSZ8081_H

#ifdef __cplusplus
 extern "C" {
#endif   
   
/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/** @addtogroup BSP
  * @{
  */ 

/** @addtogroup Component
  * @{
  */
    
/** @defgroup LANKSZ8081
  * @{
  */    
/* Exported constants --------------------------------------------------------*/
/** @defgroup LANKSZ8081_Exported_Constants LANKSZ8081 Exported Constants
  * @{
  */ 
  
/** @defgroup LANKSZ8081_Registers_Mapping LANKSZ8081 Registers Mapping
  * @{
  */ 
#define LANKSZ8081_BCR      ((uint16_t)0x0000U)
#define LANKSZ8081_BSR      ((uint16_t)0x0001U)
#define LANKSZ8081_PHYI1R   ((uint16_t)0x0002U)
#define LANKSZ8081_PHYI2R   ((uint16_t)0x0003U)
#define LANKSZ8081_ANAR     ((uint16_t)0x0004U)
#define LANKSZ8081_ANLPAR   ((uint16_t)0x0005U)
#define LANKSZ8081_ANER     ((uint16_t)0x0006U)
#define LANKSZ8081_ANNPTR   ((uint16_t)0x0007U)
#define LANKSZ8081_ANNPRR   ((uint16_t)0x0008U)

#define LANKSZ8081_PHYCTRL1  ((uint16_t)0x001EU)
#define LANKSZ8081_ICSR      ((uint16_t)0x001BU)

/**
  * @}
  */

/** @defgroup LANKSZ8081_BCR_Bit_Definition LANKSZ8081 BCR Bit Definition
  * @{
  */    
#define LANKSZ8081_BCR_SOFT_RESET         ((uint16_t)0x8000U)
#define LANKSZ8081_BCR_LOOPBACK           ((uint16_t)0x4000U)
#define LANKSZ8081_BCR_SPEED_SELECT       ((uint16_t)0x2000U)
#define LANKSZ8081_BCR_AUTONEGO_EN        ((uint16_t)0x1000U)
#define LANKSZ8081_BCR_POWER_DOWN         ((uint16_t)0x0800U)
#define LANKSZ8081_BCR_ISOLATE            ((uint16_t)0x0400U)
#define LANKSZ8081_BCR_RESTART_AUTONEGO   ((uint16_t)0x0200U)
#define LANKSZ8081_BCR_DUPLEX_MODE        ((uint16_t)0x0100U)
/**
  * @}
  */

/** @defgroup LANKSZ8081_BSR_Bit_Definition LANKSZ8081 BSR Bit Definition
  * @{
  */   
#define LANKSZ8081_BSR_100BASE_T4       ((uint16_t)0x8000U)
#define LANKSZ8081_BSR_100BASE_TX_FD    ((uint16_t)0x4000U)
#define LANKSZ8081_BSR_100BASE_TX_HD    ((uint16_t)0x2000U)
#define LANKSZ8081_BSR_10BASE_T_FD      ((uint16_t)0x1000U)
#define LANKSZ8081_BSR_10BASE_T_HD      ((uint16_t)0x0800U)
#define LANKSZ8081_BSR_100BASE_T2_FD    ((uint16_t)0x0400U)
#define LANKSZ8081_BSR_100BASE_T2_HD    ((uint16_t)0x0200U)
#define LANKSZ8081_BSR_EXTENDED_STATUS  ((uint16_t)0x0100U)
#define LANKSZ8081_BSR_AUTONEGO_CPLT    ((uint16_t)0x0020U)
#define LANKSZ8081_BSR_REMOTE_FAULT     ((uint16_t)0x0010U)
#define LANKSZ8081_BSR_AUTONEGO_ABILITY ((uint16_t)0x0008U)
#define LANKSZ8081_BSR_LINK_STATUS      ((uint16_t)0x0004U)
#define LANKSZ8081_BSR_JABBER_DETECT    ((uint16_t)0x0002U)
#define LANKSZ8081_BSR_EXTENDED_CAP     ((uint16_t)0x0001U)
/**
  * @}
  */

/** @defgroup LANKSZ8081_PHYI1R_Bit_Definition LANKSZ8081 PHYI1R Bit Definition
  * @{
  */
#define LANKSZ8081_PHYI1R_OUI_3_18           ((uint16_t)0xFFFFU)
/**
  * @}
  */

/** @defgroup LANKSZ8081_PHYI2R_Bit_Definition LANKSZ8081 PHYI2R Bit Definition
  * @{
  */
#define LANKSZ8081_PHYI2R_OUI_19_24          ((uint16_t)0xFC00U)
#define LANKSZ8081_PHYI2R_MODEL_NBR          ((uint16_t)0x03F0U)
#define LANKSZ8081_PHYI2R_REVISION_NBR       ((uint16_t)0x000FU)
/**
  * @}
  */

/** @defgroup LANKSZ8081_ANAR_Bit_Definition LANKSZ8081 ANAR Bit Definition
  * @{
  */
#define LANKSZ8081_ANAR_NEXT_PAGE               ((uint16_t)0x8000U)
#define LANKSZ8081_ANAR_REMOTE_FAULT            ((uint16_t)0x2000U)
#define LANKSZ8081_ANAR_PAUSE_OPERATION         ((uint16_t)0x0C00U)
#define LANKSZ8081_ANAR_PO_NOPAUSE              ((uint16_t)0x0000U)
#define LANKSZ8081_ANAR_PO_SYMMETRIC_PAUSE      ((uint16_t)0x0400U)
#define LANKSZ8081_ANAR_PO_ASYMMETRIC_PAUSE     ((uint16_t)0x0800U)
#define LANKSZ8081_ANAR_PO_ADVERTISE_SUPPORT    ((uint16_t)0x0C00U)
#define LANKSZ8081_ANAR_100BASE_TX_FD           ((uint16_t)0x0100U)
#define LANKSZ8081_ANAR_100BASE_TX              ((uint16_t)0x0080U)
#define LANKSZ8081_ANAR_10BASE_T_FD             ((uint16_t)0x0040U)
#define LANKSZ8081_ANAR_10BASE_T                ((uint16_t)0x0020U)
#define LANKSZ8081_ANAR_SELECTOR_FIELD          ((uint16_t)0x000FU)
/**
  * @}
  */

/** @defgroup LANKSZ8081_ANLPAR_Bit_Definition LANKSZ8081 ANLPAR Bit Definition
  * @{
  */
#define LANKSZ8081_ANLPAR_NEXT_PAGE            ((uint16_t)0x8000U)
#define LANKSZ8081_ANLPAR_REMOTE_FAULT         ((uint16_t)0x2000U)
#define LANKSZ8081_ANLPAR_PAUSE_OPERATION      ((uint16_t)0x0C00U)
#define LANKSZ8081_ANLPAR_PO_NOPAUSE           ((uint16_t)0x0000U)
#define LANKSZ8081_ANLPAR_PO_SYMMETRIC_PAUSE   ((uint16_t)0x0400U)
#define LANKSZ8081_ANLPAR_PO_ASYMMETRIC_PAUSE  ((uint16_t)0x0800U)
#define LANKSZ8081_ANLPAR_PO_ADVERTISE_SUPPORT ((uint16_t)0x0C00U)
#define LANKSZ8081_ANLPAR_100BASE_TX_FD        ((uint16_t)0x0100U)
#define LANKSZ8081_ANLPAR_100BASE_TX           ((uint16_t)0x0080U)
#define LANKSZ8081_ANLPAR_10BASE_T_FD          ((uint16_t)0x0040U)
#define LANKSZ8081_ANLPAR_10BASE_T             ((uint16_t)0x0020U)
#define LANKSZ8081_ANLPAR_SELECTOR_FIELD       ((uint16_t)0x000FU)
/**
  * @}
  */

/** @defgroup LANKSZ8081_ANER_Bit_Definition LANKSZ8081 ANER Bit Definition
  * @{
  */
#define LANKSZ8081_ANER_RX_NP_LOCATION_ABLE    ((uint16_t)0x0040U)
#define LANKSZ8081_ANER_RX_NP_STORAGE_LOCATION ((uint16_t)0x0020U)
#define LANKSZ8081_ANER_PARALLEL_DETECT_FAULT  ((uint16_t)0x0010U)
#define LANKSZ8081_ANER_LP_NP_ABLE             ((uint16_t)0x0008U)
#define LANKSZ8081_ANER_NP_ABLE                ((uint16_t)0x0004U)
#define LANKSZ8081_ANER_PAGE_RECEIVED          ((uint16_t)0x0002U)
#define LANKSZ8081_ANER_LP_AUTONEG_ABLE        ((uint16_t)0x0001U)
/**
  * @}
  */

/** @defgroup LANKSZ8081_ANNPTR_Bit_Definition LANKSZ8081 ANNPTR Bit Definition
  * @{
  */
#define LANKSZ8081_ANNPTR_NEXT_PAGE         ((uint16_t)0x8000U)
#define LANKSZ8081_ANNPTR_MESSAGE_PAGE      ((uint16_t)0x2000U)
#define LANKSZ8081_ANNPTR_ACK2              ((uint16_t)0x1000U)
#define LANKSZ8081_ANNPTR_TOGGLE            ((uint16_t)0x0800U)
#define LANKSZ8081_ANNPTR_MESSAGGE_CODE     ((uint16_t)0x07FFU)
/**
  * @}
  */

/** @defgroup LANKSZ8081_ANNPRR_Bit_Definition LANKSZ8081 ANNPRR Bit Definition
  * @{
  */
#define LANKSZ8081_ANNPTR_NEXT_PAGE         ((uint16_t)0x8000U)
#define LANKSZ8081_ANNPRR_ACK               ((uint16_t)0x4000U)
#define LANKSZ8081_ANNPRR_MESSAGE_PAGE      ((uint16_t)0x2000U)
#define LANKSZ8081_ANNPRR_ACK2              ((uint16_t)0x1000U)
#define LANKSZ8081_ANNPRR_TOGGLE            ((uint16_t)0x0800U)
#define LANKSZ8081_ANNPRR_MESSAGGE_CODE     ((uint16_t)0x07FFU)
/**
  * @}
  */


/** @defgroup LANKSZ8081_PHYCTRL1_Bit_Definition LANKSZ8081 PHYCTRL1 Bit Definition
* @{
*/
#define LANKSZ8081_PHYCTRL1_AUTONEGO_ONGOING ((uint16_t)0x0003U)
#define LANKSZ8081_PHYCTRL1_HCDSPEEDMASK     ((uint16_t)0x0007U)
#define LANKSZ8081_PHYCTRL1_10BT_HD          ((uint16_t)0x0001U)
#define LANKSZ8081_PHYCTRL1_10BT_FD          ((uint16_t)0x0005U)
#define LANKSZ8081_PHYCTRL1_100BTX_HD        ((uint16_t)0x0002U)
#define LANKSZ8081_PHYCTRL1_100BTX_FD        ((uint16_t)0x0006U)
/**
* @}
*/

 /** @defgroup LANKSZ8081_IMR_ISFR_Bit_Definition LANKSZ8081 IMR ISFR Bit Definition
   * @{
   */
#define LANKSZ8081_INT_16      ((uint16_t)0x000FU)
#define LANKSZ8081_INT_15      ((uint16_t)0x000EU)
#define LANKSZ8081_INT_14      ((uint16_t)0x000DU)
#define LANKSZ8081_INT_13      ((uint16_t)0x000CU)
#define LANKSZ8081_INT_12      ((uint16_t)0x000BU)
#define LANKSZ8081_INT_11      ((uint16_t)0x000AU)
#define LANKSZ8081_INT_10      ((uint16_t)0x0009U)
#define LANKSZ8081_INT_9       ((uint16_t)0x0008U)
#define LANKSZ8081_INT_8       ((uint16_t)0x0007U)
#define LANKSZ8081_INT_7       ((uint16_t)0x0006U)
#define LANKSZ8081_INT_6       ((uint16_t)0x0005U)
#define LANKSZ8081_INT_5       ((uint16_t)0x0004U)
#define LANKSZ8081_INT_4       ((uint16_t)0x0003U)
#define LANKSZ8081_INT_3       ((uint16_t)0x0002U)
#define LANKSZ8081_INT_2       ((uint16_t)0x0001U)
#define LANKSZ8081_INT_1       ((uint16_t)0x0000U)
 /**
   * @}
   */



    
/** @defgroup LANKSZ8081_Status LANKSZ8081 Status
  * @{
  */

#define  LANKSZ8081_STATUS_READ_ERROR            ((int32_t)-5)
#define  LANKSZ8081_STATUS_WRITE_ERROR           ((int32_t)-4)
#define  LANKSZ8081_STATUS_ADDRESS_ERROR         ((int32_t)-3)
#define  LANKSZ8081_STATUS_RESET_TIMEOUT         ((int32_t)-2)
#define  LANKSZ8081_STATUS_ERROR                 ((int32_t)-1)
#define  LANKSZ8081_STATUS_OK                    ((int32_t) 0)
#define  LANKSZ8081_STATUS_LINK_DOWN             ((int32_t) 1)
#define  LANKSZ8081_STATUS_100MBITS_FULLDUPLEX   ((int32_t) 2)
#define  LANKSZ8081_STATUS_100MBITS_HALFDUPLEX   ((int32_t) 3)
#define  LANKSZ8081_STATUS_10MBITS_FULLDUPLEX    ((int32_t) 4)
#define  LANKSZ8081_STATUS_10MBITS_HALFDUPLEX    ((int32_t) 5)
#define  LANKSZ8081_STATUS_AUTONEGO_NOTDONE      ((int32_t) 6)
/**
  * @}
  */

/**
  * @}
  */

/* Exported types ------------------------------------------------------------*/ 
/** @defgroup LANKSZ8081_Exported_Types LANKSZ8081 Exported Types
  * @{
  */
typedef int32_t  (*lanKSZ8081_Init_Func) (void);
typedef int32_t  (*lanKSZ8081_DeInit_Func) (void);
typedef int32_t  (*lanKSZ8081_ReadReg_Func)   (uint32_t, uint32_t, uint32_t *);
typedef int32_t  (*lanKSZ8081_WriteReg_Func)  (uint32_t, uint32_t, uint32_t);
typedef int32_t  (*lanKSZ8081_GetTick_Func)  (void);

typedef struct 
{                   
  lanKSZ8081_Init_Func      Init;
  lanKSZ8081_DeInit_Func    DeInit;
  lanKSZ8081_WriteReg_Func  WriteReg;
  lanKSZ8081_ReadReg_Func   ReadReg;
  lanKSZ8081_GetTick_Func   GetTick;
} lanKSZ8081_IOCtx_t;

  
typedef struct 
{
  uint32_t            DevAddr;
  uint32_t            Is_Initialized;
  lanKSZ8081_IOCtx_t     IO;
  void               *pData;
}lanKSZ8081_Object_t;
/**
  * @}
  */ 

/* Exported macro ------------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/
/** @defgroup LANKSZ8081_Exported_Functions LANKSZ8081 Exported Functions
  * @{
  */
int32_t LANKSZ8081_RegisterBusIO(lanKSZ8081_Object_t *pObj, lanKSZ8081_IOCtx_t *ioctx);
int32_t LANKSZ8081_Init(lanKSZ8081_Object_t *pObj);
int32_t LANKSZ8081_DeInit(lanKSZ8081_Object_t *pObj);
int32_t LANKSZ8081_DisablePowerDownMode(lanKSZ8081_Object_t *pObj);
int32_t LANKSZ8081_EnablePowerDownMode(lanKSZ8081_Object_t *pObj);
int32_t LANKSZ8081_StartAutoNego(lanKSZ8081_Object_t *pObj);
int32_t LANKSZ8081_GetLinkState(lanKSZ8081_Object_t *pObj);
int32_t LANKSZ8081_SetLinkState(lanKSZ8081_Object_t *pObj, uint32_t LinkState);
int32_t LANKSZ8081_EnableLoopbackMode(lanKSZ8081_Object_t *pObj);
int32_t LANKSZ8081_DisableLoopbackMode(lanKSZ8081_Object_t *pObj);
int32_t LANKSZ8081_EnableIT(lanKSZ8081_Object_t *pObj, uint32_t Interrupt);
int32_t LANKSZ8081_DisableIT(lanKSZ8081_Object_t *pObj, uint32_t Interrupt);
int32_t LANKSZ8081_ClearIT(lanKSZ8081_Object_t *pObj, uint32_t Interrupt);
int32_t LANKSZ8081_GetITStatus(lanKSZ8081_Object_t *pObj, uint32_t Interrupt);
/**
  * @}
  */ 

#ifdef __cplusplus
}
#endif
#endif /* LANKSZ8081_H */


/**
  * @}
  */ 

/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */       
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

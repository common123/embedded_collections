# Trip hazards

We have connected to an STM32H730 the PHY KSZ8081RND type(RMII_MODE).
The STM32 needs a 50MHZ clock(RMII_REF_CLOCK) . 
The PHY type must NOT be used with a Chrystal and only uses PIN8/XI. However, nothing comes out at the RMII_REF_CLOCK output in 50MHz mode. The trick here is to simply attach the oscillator to XI(PHY) and RMII_REF_CLOCK(input STM32) at the same time. 
